package br.com.cromossomonerd.app.API

import br.com.cromossomonerd.app.Models.Post
import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient {

    private val service: RetrofitService

    init {

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
        httpClient.addInterceptor(logging)

        val retrofit = Retrofit.Builder()
                .baseUrl("http://depositonerd.com.br")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build()
        service = retrofit.create(RetrofitService::class.java)
    }

    fun getPosts(page: Int?, cat: Int?, callback: Callback<List<Post>>) {
        val call = service.getPosts(page, cat)
        call.enqueue(callback)
    }
}
