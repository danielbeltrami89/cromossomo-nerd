package br.com.cromossomonerd.app

import android.os.Bundle

import androidx.drawerlayout.widget.DrawerLayout

import androidx.appcompat.app.AppCompatActivity

import android.view.Menu
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.cromossomonerd.app.Adapters.RecyclerViewAdapter
import br.com.cromossomonerd.app.Models.Post
import br.com.cromossomonerd.app.API.RetrofitClient
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.toolbar_padrao.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    lateinit var recyclerView: RecyclerView
    lateinit var recyclerAdapter: RecyclerViewAdapter
    private var posts = ArrayList<Post>()
    private var progressBar: ProgressBar? = null
    private var drawer: DrawerLayout? = null

    private var isLoading = false
    private var pagina: Int = 1
    private var categoria: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initToolbar()
        initRecyclerView()
        buscaPosts(pagina, categoria)
    }

    private fun initToolbar() {
        val toolbar = tool_bar
        setSupportActionBar(toolbar)

//        drawer = findViewById(R.id.drawer_layout)
//        val toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.Open, R.string.Close)
//        drawer.addDrawerListener(toggle)
//        toggle.syncState()

        //val navigationView = findViewById<NavigationView>(R.id.nav_view)
        //navigationView.setNavigationItemSelectedListener(this)

        // Exibir a versao do app no sidebar
//        val headerView = navigationView.getHeaderView(0)
//        val nav_user = headerView.findViewById(R.id.txtVersao)
//        nav_user.setText("Versão " + BuildConfig.VERSION_NAME)
    }

    private fun initRecyclerView() {
        recyclerView = recicler_view
        recyclerView.layoutManager = LinearLayoutManager(this)
        progressBar = progress_bar

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == posts.size - 1) {
                        pagina++
                        buscaMaisPosts(pagina, categoria)

                    }
                }
            }
        })
    }

    private fun buscaPosts(page: Int?, cat: Int?) {

        progressBar?.setVisibility(VISIBLE)
        isLoading = true

        RetrofitClient().getPosts(page, cat, object : Callback<List<Post>> {
            override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                progressBar?.setVisibility(GONE)
                isLoading = false
            }

            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                progressBar?.setVisibility(GONE)
                isLoading = false

                response.isSuccessful.let {
                    posts = response.body() as ArrayList<Post>
                    recyclerAdapter = RecyclerViewAdapter(posts, this@MainActivity)
                    recyclerView.adapter = recyclerAdapter
                }
            }
        })
    }

    private fun buscaMaisPosts(page: Int?, cat: Int?) {

        isLoading = true
        posts.add(Post())
        //recyclerView.adapter = RecyclerViewAdapter(posts, this@MainActivity)
        recyclerAdapter.notifyItemInserted(posts.size - 1)

        RetrofitClient().getPosts(page, cat, object : Callback<List<Post>> {
            override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                isLoading = false
            }

            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {

                response.isSuccessful.let {

                    posts.removeAt(posts.size - 1)
                    val scrollPosition = posts.size
                    recyclerAdapter.notifyItemRemoved(scrollPosition)

                    for (postItem in response.body() as ArrayList<Post>) {
                        posts.add(postItem)
                    }
                    recyclerAdapter.notifyDataSetChanged()
                    isLoading = false
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

}
