package br.com.cromossomonerd.app.API

import br.com.cromossomonerd.app.Models.Post
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RetrofitService {

    @GET("/wp-json/wp/v2/posts/")
    fun getPosts(
            @Query("page") pagina: Int?,
            @Query("categories") categoria: Int?
    ): Call<List<Post>>

    @GET("/wp-json/wp/v2/posts/{id}")
    fun getPost(
            @Path("id") id: String
    ): Call<Post>
}
