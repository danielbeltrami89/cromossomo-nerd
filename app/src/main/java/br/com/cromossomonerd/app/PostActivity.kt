package br.com.cromossomonerd.app

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.TextView

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.toolbar_padrao.*

class PostActivity : AppCompatActivity() {

    private var idPost = ""
    private val progressBar: ProgressBar? = null
    private val textTitle: TextView? = null
    private var webView: WebView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)

        if (intent.getStringExtra("ID_POST") != null)
            idPost = intent.getStringExtra("ID_POST")

        initToolbar()
        initRes()
        setupWebView()
        //buscaPost();
    }

    private fun initToolbar() {
        val toolbar = tool_bar
        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayShowTitleEnabled(false)
        }
    }

    private fun initRes() {
        //textTitle = findViewById(R.id.textTitle);
        //progressBar = findViewById(R.id.progressbar);
    }

    private fun setupWebView() {
        webView = findViewById(R.id.webview)
        val html = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                idPost +
                "</body>\n" +
                "</html>"

        //webView.loadData(html, "text/html", "UTF-8");

        webView!!.loadUrl(idPost)
        webView!!.settings.javaScriptEnabled = true
        webView!!.webViewClient = WebViewClient()
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        finish()
    }

    companion object {

        private val TAG = "PostActivity"
    }

}
