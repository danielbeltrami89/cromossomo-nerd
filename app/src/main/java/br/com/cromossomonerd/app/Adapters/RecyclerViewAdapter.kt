package br.com.cromossomonerd.app.Adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

import com.squareup.picasso.Picasso

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

import br.com.cromossomonerd.app.Models.Post
import br.com.cromossomonerd.app.PostActivity
import br.com.cromossomonerd.app.R
import kotlinx.android.synthetic.main.cardview.view.*

class RecyclerViewAdapter(val posts: List<Post>, val activity: Activity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_LOADING = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(activity).inflate(R.layout.cardview, parent, false)
            return ViewHolder(view)
        } else {
            val view = LayoutInflater.from(activity).inflate(R.layout.item_load, parent, false)
            return ViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is ViewHolder) {
            val post = posts[position]
            holder?.let {
                it.bindView(post)
            }
            //populateItemRows(holder, position)
        } else if (holder is LoadingViewHolder) {
            showLoadingView(holder, position)
        }
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (posts[position] == null) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    private inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var progressBar: ProgressBar

        init {
            progressBar = itemView.findViewById(R.id.progressBar)
        }
    }

    private fun showLoadingView(viewHolder: LoadingViewHolder, position: Int) {
        //ProgressBar would be displayed
    }

    fun dateConvert(data: String?) : String{

        var spf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        var newDate: Date? = null
        try {
            newDate = spf.parse(data)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val localeBR = Locale("pt", "BR")
        spf = SimpleDateFormat("d MMM yyyy - HH:mm", localeBR)

        return spf.format(newDate)
    }

    inner class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(post: Post){

            val titleText = itemView.card_view_image_title
            val imagemView = itemView.card_view_image
            val catText = itemView.card_view_category
            val cat2Text = itemView.card_view_category2
            val dataText = itemView.card_view_data

            val titulo = post.title
            titleText.text = titulo?.rendered

            val categoria = post.category?.get(0)
            catText.text = categoria?.name

            dataText.text = dateConvert(post.date)

            val imgUrl = post.fimg_url
            if (imgUrl != null) {
                Picasso.with(activity)
                        .load(imgUrl)
                        .into(imagemView)
            } else {
                imagemView.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.sem_imagem))
            }

            itemView.setOnClickListener {
                                val link = post.guid
                activity.startActivity(Intent(activity, PostActivity::class.java).putExtra("ID_POST", link?.rendered))
            }
        }
    }
}


