package br.com.cromossomonerd.app.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Post {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("date_gmt")
    var dateGmt: String? = null

    @SerializedName("guid")
    var guid: Guid? = null

    @SerializedName("modified")
    var modified: String? = null

    @SerializedName("modified_gmt")
    var modifiedGmt: String? = null

    @SerializedName("slug")
    var slug: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("link")
    var link: String? = null

    @SerializedName("title")
    var title: Title? = null

    @SerializedName("content")
    var content: Content? = null

    @SerializedName("excerpt")
    var excerpt: Excerpt? = null

    @SerializedName("author")
    var author: Int? = null

    @SerializedName("featured_media")
    var featuredMedia: Int? = null

    @SerializedName("comment_status")
    var commentStatus: String? = null

    @SerializedName("ping_status")
    var pingStatus: String? = null

    @SerializedName("sticky")
    var sticky: Boolean? = null

    @SerializedName("template")
    var template: String? = null

    @SerializedName("format")
    var format: String? = null

    @SerializedName("meta")
    var meta: List<Meta>? = null

    @SerializedName("categories")
    var categories: List<Int>? = null

    @SerializedName("tags")
    var tags: List<Int>? = null

    @SerializedName("_links")
    var links: Links? = null

    @SerializedName("category")
    var category: List<Category>? = null

    @SerializedName("fimg_url")
    var fimg_url: String? = null

    inner class About {

        @SerializedName("href")
        var href: String? = null

    }

    inner class Author {

        @SerializedName("embeddable")
        var embeddable: Boolean? = null

        @SerializedName("href")
        var href: String? = null

    }

    inner class Collection {

        @SerializedName("href")
        var href: String? = null

    }

    inner class Content {

        @SerializedName("rendered")
        var rendered: String? = null

        @SerializedName("protected")
        var protected: Boolean? = null

    }

    inner class Cury {

        @SerializedName("name")
        var name: String? = null

        @SerializedName("href")
        var href: String? = null

        @SerializedName("templated")
        var templated: Boolean? = null

    }

    inner class Excerpt {

        @SerializedName("rendered")
        var rendered: String? = null

        @SerializedName("protected")
        var protected: Boolean? = null

    }

    inner class Guid {

        @SerializedName("rendered")
        var rendered: String? = null

    }

    inner class Img {

        @SerializedName("alt_text")
        var altText: String? = null

        @SerializedName("src")
        var src: String? = null

        @SerializedName("width")
        var width: Int? = null

        @SerializedName("height")
        var height: Int? = null

    }


    inner class Links {

        @SerializedName("self")
        var self: List<Self>? = null

        @SerializedName("collection")
        var collection: List<Collection>? = null

        @SerializedName("about")
        var about: List<About>? = null

        @SerializedName("author")
        var author: List<Author>? = null

        @SerializedName("replies")
        var replies: List<Reply>? = null

        @SerializedName("version-history")
        var versionHistory: List<VersionHistory>? = null

        @SerializedName("predecessor-version")
        var predecessorVersion: List<PredecessorVersion>? = null

        @SerializedName("wp:featuredmedia")
        var wpFeaturedmedia: List<WpFeaturedmedium>? = null

        @SerializedName("wp:attachment")
        var wpAttachment: List<WpAttachment>? = null

        @SerializedName("wp:term")
        var wpTerm: List<WpTerm>? = null

        @SerializedName("curies")
        var curies: List<Cury>? = null

    }

    inner class Meta {

        @SerializedName("spay_email")
        var spayEmail: String? = null

        @SerializedName("jetpack_publicize_message")
        var jetpackPublicizeMessage: String? = null

    }

    inner class PredecessorVersion {

        @SerializedName("id")
        var id: Int? = null

        @SerializedName("href")
        var href: String? = null

    }

    inner class Reply {

        @SerializedName("embeddable")
        var embeddable: Boolean? = null

        @SerializedName("href")
        var href: String? = null

    }

    inner class Self {

        @SerializedName("href")
        var href: String? = null

    }

    inner class Title {

        @SerializedName("rendered")
        var rendered: String? = null

    }

    inner class UrlMeta {

        @SerializedName("origin")
        var origin: Int? = null

        @SerializedName("position")
        var position: Int? = null

    }

    inner class VersionHistory {

        @SerializedName("count")
        var count: Int? = null

        @SerializedName("href")
        var href: String? = null

    }

    inner class WpAttachment {

        @SerializedName("href")
        var href: String? = null

    }

    inner class WpFeaturedmedium {

        @SerializedName("embeddable")
        var embeddable: Boolean? = null

        @SerializedName("href")
        var href: String? = null

    }

    inner class WpTerm {

        @SerializedName("taxonomy")
        var taxonomy: String? = null

        @SerializedName("embeddable")
        var embeddable: Boolean? = null

        @SerializedName("href")
        var href: String? = null

    }

    inner class Category {

        @SerializedName("term_id")
        var termId: Int? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("slug")
        var slug: String? = null

        @SerializedName("term_group")
        var termGroup: Int? = null

        @SerializedName("term_taxonomy_id")
        var termTaxonomyId: Int? = null

        @SerializedName("taxonomy")
        var taxonomy: String? = null

        @SerializedName("description")
        var description: String? = null

        @SerializedName("parent")
        var parent: Int? = null

        @SerializedName("count")
        var count: Int? = null

        @SerializedName("filter")
        var filter: String? = null

        @SerializedName("cat_ID")
        var catID: Int? = null

        @SerializedName("category_count")
        var categoryCount: Int? = null

        @SerializedName("category_description")
        var categoryDescription: String? = null

        @SerializedName("cat_name")
        var catName: String? = null

        @SerializedName("category_nicename")
        var categoryNicename: String? = null

        @SerializedName("category_parent")
        var categoryParent: Int? = null

    }
}


